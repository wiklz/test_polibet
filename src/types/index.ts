export enum RoutesEnum {
  contacts = "/test_polibet/contacts",
  prices = "/test_polibet/prices",
  products = "/test_polibet/products",
  services = "/test_polibet/services",
  home = "/test_polibet",
  email = "mailto:pskpolibet@rambler.ru",
  phone = "tel:+79511357712",
  address = "https://yandex.ru/maps/-/CCUBzZxSOC",
}

export enum TitleLevels {
  level1 = "1",
  level2 = "2",
  level3 = "3",
  level4 = "4",
  level5 = "5",
  level6 = "6",
}

export type HeaderLinksType = {
  url: RoutesEnum;
  text: string;
}[];
