import React from "react";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import Home from "./Containers/Home";
import Prices from "./Containers/Prices";
import Products from "./Containers/Products";
import Services from "./Containers/Services";
import Layout from "./HOCs/Layout";
import { RoutesEnum } from "./types";

const App: React.FC = () => {
  return (
    <BrowserRouter>
      <Layout>
        <Routes>
          <Route path={RoutesEnum.services} element={<Services />} />
          <Route path={RoutesEnum.products} element={<Products />} />
          <Route path={RoutesEnum.prices} element={<Prices />} />
          <Route path={RoutesEnum.home} element={<Home />} />
        </Routes>
      </Layout>
    </BrowserRouter>
  );
};

export default App;
