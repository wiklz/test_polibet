import React from "react";
import ImageCard from "../../Components/ImageCard";
import PageTitle from "../../Components/UI/PageTitle";
import PageWrapper from "../../Components/UI/PageWrapper";
import { pricesData } from "../../consts";
import { TitleLevels } from "../../types";
import classes from "./styles.module.scss";

const Prices: React.FC = () => (
  <PageWrapper>
    <div className={classes.Wrapper}>
      <PageTitle className={classes.Title}>{pricesData.title}</PageTitle>
      <ImageCard
        text={
          <PageTitle level={TitleLevels.level3} className={classes.Name}>
            {pricesData.TIB.name}: {pricesData.TIB.price}/м<sup>3</sup>
          </PageTitle>
        }
        src={pricesData.TIB.img}
        alt={pricesData.TIB.name}
      />
      <ImageCard
        text={
          <PageTitle level={TitleLevels.level3} className={classes.Name}>
            {pricesData.PIC.name}: {pricesData.PIC.price}/м<sup>3</sup>
          </PageTitle>
        }
        src={pricesData.PIC.img}
        alt={pricesData.PIC.name}
      />
    </div>
  </PageWrapper>
);

export default Prices;
