import React from "react";
import List from "../../Components/UI/List";
import PageTitle from "../../Components/UI/PageTitle";
import PageWrapper from "../../Components/UI/PageWrapper";
import Text from "../../Components/UI/Text";
import { TitleLevels } from "../../types";
import { productsData } from "../../consts";
import classes from "./styles.module.scss";

const Products: React.FC = () => (
  <PageWrapper>
    <PageTitle className={classes.Title}>{productsData.title}</PageTitle>
    <div className={classes.ProductContainer}>
      <div className={classes.Col}>
        <img
          src={productsData.TIB.img}
          alt={productsData.TIB.title}
          className={classes.ProductImage}
        />
      </div>
      <div className={classes.Col}>
        <PageTitle level={TitleLevels.level3} className={classes.ProductTitle}>
          {productsData.TIB.title}
        </PageTitle>
        <Text className={classes.Text}>{productsData.TIB.description}</Text>
        <Text className={classes.Text}>{productsData.TIB.prePointsText}</Text>
        <List items={productsData.TIB.points} />
      </div>
    </div>
    <div className={classes.ProductContainer}>
      <div className={classes.Col}>
        <img
          src={productsData.PIC.img}
          alt={productsData.PIC.title}
          className={classes.ProductImage}
        />
      </div>
      <div className={classes.Col}>
        <PageTitle level={TitleLevels.level3} className={classes.ProductTitle}>
          {productsData.PIC.title}
        </PageTitle>
        <Text>{productsData.PIC.description}</Text>
      </div>
    </div>
  </PageWrapper>
);

export default Products;
