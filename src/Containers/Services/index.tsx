import React from "react";
import PageTitle from "../../Components/UI/PageTitle";
import PageWrapper from "../../Components/UI/PageWrapper";
import { TitleLevels } from "../../types";
import { servicesData } from "../../consts";
import ImageCard from "../../Components/ImageCard";
import classes from "./styles.module.scss";

const Services: React.FC = () => (
  <PageWrapper>
    <div className={classes.Wrapper}>
      <PageTitle className={classes.Title}>{servicesData.title}</PageTitle>
      {servicesData.list.map(({ text, imgSrc }) => (
        <ImageCard
          key={text}
          text={
            <PageTitle level={TitleLevels.level3} className={classes.Name}>
              {text}
            </PageTitle>
          }
          src={imgSrc}
          alt={text}
        />
      ))}
    </div>
  </PageWrapper>
);

export default Services;
