import React from "react";
import HeadSlider from "../../Components/HeadSlider";
import List from "../../Components/UI/List";
import PageTitle from "../../Components/UI/PageTitle";
import PageWrapper from "../../Components/UI/PageWrapper";
import Text from "../../Components/UI/Text";
import { homeData } from "../../consts";
import classes from "./styles.module.scss";

const Home: React.FC = () => (
  <PageWrapper>
    <HeadSlider />
    <div className={classes.Content}>
      <PageTitle className={classes.Title}>{homeData.title}</PageTitle>
      <Text className={classes.Text}>{homeData.text_secondary}</Text>
      <List items={homeData.points} />
    </div>
  </PageWrapper>
);

export default Home;
