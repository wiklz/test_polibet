import React from "react";
import { RoutesEnum } from "../../types";
import AddressIcon from "../UI/Icons/AddressIcon";
import MailIcon from "../UI/Icons/MailIcon";
import PhoneIcon from "../UI/Icons/PhoneIcon";
import Logo from "../UI/Logo";
import classes from "./styles.module.scss";

const Footer: React.FC = () => {
  return (
    <footer className={classes.Footer}>
      <div className={classes.Content}>
        <div className={classes.LogoWrapper}>
          <Logo />
        </div>
        <div className={classes.Contacts}>
          <div className={classes.LinkWrapper}>
            <PhoneIcon size={16} />
            <a
              href={RoutesEnum.phone}
              target="_blank"
              rel="noreferrer"
              className={classes.Link}
            >
              +7 (951) 135-77-12
            </a>
          </div>
          <div className={classes.LinkWrapper}>
            <MailIcon size={16} />
            <a
              href={RoutesEnum.email}
              target="_blank"
              rel="noreferrer"
              className={classes.Link}
            >
              pskpolibet@rambler.ru
            </a>
          </div>
          <div className={classes.LinkWrapper}>
            <AddressIcon size={16} />
            <a
              href={RoutesEnum.address}
              target="_blank"
              rel="noreferrer"
              className={classes.Link}
            >
              г. Белгород, ул. К.Заслонова, д. 195, пом.17
            </a>
          </div>
        </div>
      </div>
    </footer>
  );
};

export default Footer;
