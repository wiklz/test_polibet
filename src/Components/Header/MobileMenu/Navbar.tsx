import React from "react";
import { useLocation, useNavigate } from "react-router-dom";
import { MOBILE_MENU_ID } from "../../../consts";
import { HeaderLinksType, RoutesEnum } from "../../../types";
import classes from "./styles.module.scss";

interface IMobileNavbarProps {
  isOpen: boolean;
  close: () => void;
  data: HeaderLinksType;
}

const MobileNavbar: React.FC<IMobileNavbarProps> = ({
  isOpen,
  data,
  close,
}) => {
  const { pathname } = useLocation();
  const navigate = useNavigate();

  const handleClick = (url: RoutesEnum) => {
    close();
    navigate(url);
  };

  return (
    <div className={`${classes.Navbar} ${isOpen ? classes.Open : ""}`} id={MOBILE_MENU_ID}>
      <span
        onClick={() => handleClick(RoutesEnum.home)}
        className={`${classes.NavLink} ${
          pathname === RoutesEnum.home ? classes.Active : ""
        }`}
      >
        Главная
      </span>
      {data.map((link) => (
        <span
          key={link.url}
          onClick={() => handleClick(link.url)}
          className={`${classes.NavLink} ${
            pathname === link.url ? classes.Active : ""
          }`}
        >
          {link.text}
        </span>
      ))}
    </div>
  );
};

export default MobileNavbar;
