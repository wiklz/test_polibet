import React from "react";
import classes from "./styles.module.scss";

interface IBurgerBtnProps {
  isOpen: boolean;
  onClick: () => void;
}

const BurgerBtn: React.FC<IBurgerBtnProps> = ({ isOpen, onClick }) => {
  return (
    <button className={classes.Button} onClick={onClick}>
      <svg
        viewBox="0 0 56 56"
        fill="none"
        xmlns="http://www.w3.org/2000/svg"
        className={`${classes.Icon} ${isOpen ? classes.Open : ""}`}
      >
        <line
          className={classes.TopLine}
          x1="5"
          y1="36"
          x2="51"
          y2="36"
          stroke="black"
          strokeWidth="2"
        />
        <line
          className={classes.BottomLine}
          x1="5"
          y1="18"
          x2="51"
          y2="18"
          stroke="black"
          strokeWidth="2"
        />
      </svg>
    </button>
  );
};

export default BurgerBtn;
