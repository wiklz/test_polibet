import React, { useEffect } from "react";
import { disableBodyScroll, enableBodyScroll } from "body-scroll-lock";
import { HeaderLinksType } from "../../../types";
import BurgerBtn from "./BurgerBtn";
import MobileNavbar from "./Navbar";
import { MOBILE_MENU_ID } from "../../../consts";

interface IMobileMenuProps {
  data: HeaderLinksType;
  isOpen: boolean;
  toggleOpen: (val: boolean) => void;
}

const MobileMenu: React.FC<IMobileMenuProps> = ({
  data,
  isOpen,
  toggleOpen,
}) => {
  useEffect(() => {
    const mobileMenuEl = document.querySelector(`#${MOBILE_MENU_ID}`);
    if (isOpen && mobileMenuEl) {
      disableBodyScroll(mobileMenuEl);
    }
    if (!isOpen && mobileMenuEl) {
      enableBodyScroll(mobileMenuEl);
    }
  }, [isOpen]);

  return (
    <>
      <BurgerBtn isOpen={isOpen} onClick={() => toggleOpen(!isOpen)} />
      <MobileNavbar
        isOpen={isOpen}
        data={data}
        close={() => toggleOpen(false)}
      />
    </>
  );
};

export default MobileMenu;
