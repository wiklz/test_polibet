import React, { useEffect, useRef, useState } from "react";
import { Link, useLocation } from "react-router-dom";
import Logo from "../UI/Logo";
import MobileMenu from "./MobileMenu";
import { routesData } from "../../consts";
import classes from "./styles.module.scss";

const SHADOW_APPEARANCE_TRESHOLD = 15;

const Header: React.FC = () => {
  const { pathname } = useLocation();

  const headerRef = useRef<HTMLElement | null>(null);

  const [isOpenMobileMenu, setIsOpenMobileMenu] = useState(false);

  useEffect(() => {
    const toggleShadow = () => {
      if (
        window.scrollY > SHADOW_APPEARANCE_TRESHOLD &&
        !headerRef.current?.classList.contains(classes.Shadow)
      ) {
        headerRef.current?.classList.add(classes.Shadow);
      }

      if (
        window.scrollY <= SHADOW_APPEARANCE_TRESHOLD &&
        headerRef.current?.classList.contains(classes.Shadow)
      ) {
        headerRef.current?.classList.remove(classes.Shadow);
      }
    };
    document.addEventListener("scroll", toggleShadow, { passive: true });

    return () => {
      document.removeEventListener("scroll", toggleShadow);
    };
  }, []);

  return (
    <header ref={headerRef} className={classes.Header}>
      <nav className={classes.Navbar}>
        <MobileMenu
          data={routesData}
          isOpen={isOpenMobileMenu}
          toggleOpen={setIsOpenMobileMenu}
        />
        <Logo clickCallback={() => setIsOpenMobileMenu(false)} />
        <ul className={classes.Links}>
          {routesData.map((route) => (
            <li key={route.url}>
              <Link
                to={route.url}
                className={`${classes.Link} ${
                  pathname === route.url ? classes.Active : ""
                }`}
              >
                {route.text}
              </Link>
            </li>
          ))}
        </ul>
      </nav>
    </header>
  );
};

export default Header;
