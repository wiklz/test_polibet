import React from "react";
import Slider from "react-slick";
import image1 from "../../assets/img/slider/1.jpeg";
import image2 from "../../assets/img/slider/2.jpeg";
import image3 from "../../assets/img/slider/3.jpeg";
import classes from "./styles.module.scss";

const HeadSlider: React.FC = () => {
  const urls = [image1, image2, image3];
  return (
    <div className={classes.Container}>
      <Slider autoplay draggable arrows={false} dots infinite speed={500}>
        {urls.map((url, index) => (
          <img
            className={classes.Image}
            src={url}
            key={index}
            alt={`${index}`}
          />
        ))}
      </Slider>
    </div>
  );
};

export default HeadSlider;
