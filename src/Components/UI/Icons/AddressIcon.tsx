import React from "react";
import { IIconProps } from "./consts";

const AddressIcon: React.FC<IIconProps> = ({
  color = "currentColor",
  size = 32,
}) => {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      version="1.1"
      xmlnsXlink="http://www.w3.org/1999/xlink"
      width={size}
      height={size}
      x="0"
      y="0"
      viewBox="0 0 24 24"
      xmlSpace="preserve"
    >
      <g>
        <path
          xmlns="http://www.w3.org/2000/svg"
          d="m12 1a7.008 7.008 0 0 0 -7 7c0 5.243 5.916 14.177 6.168 14.555a1 1 0 0 0 1.664 0c.252-.378 6.168-9.312 6.168-14.555a7.008 7.008 0 0 0 -7-7zm0 10a3 3 0 1 1 3-3 3 3 0 0 1 -3 3z"
          fill={color}
          data-original="#000000"
        ></path>
      </g>
    </svg>
  );
};

export default AddressIcon;
