import React from "react";
import { Link } from "react-router-dom";
import { RoutesEnum } from "../../../types";
import Text from "../Text";
import logo from "./logo.png";
import classes from "./styles.module.scss";

interface ILogoProps {
  clickCallback?: () => void;
}

const Logo: React.FC<ILogoProps> = ({ clickCallback }) => {
  return (
    <Link
      to={RoutesEnum.home}
      onClick={clickCallback}
      className={classes.LogoWrapper}
    >
      <img src={logo} alt="LOGO" className={classes.Logo} />
      <div>
        <Text className={classes.LogoText}>ООО&nbsp;ПСК ПолиБет</Text>
      </div>
    </Link>
  );
};

export default Logo;
