import React from "react";
import classes from "./styles.module.scss";

const PageWrapper: React.FC = ({ children }) => {
  return <main className={classes.PageWrapper}>{children}</main>;
};

export default PageWrapper;
