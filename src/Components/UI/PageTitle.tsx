import React from "react";
import { TitleLevels } from "../../types";

interface IPageTitleProps {
  level?: TitleLevels;
  className?: string;
}

const PageTitle: React.FC<IPageTitleProps> = ({
  level = TitleLevels.level1,
  className,
  children,
}) => {
  return {
    [TitleLevels.level1]: <h1 className={className}>{children}</h1>,
    [TitleLevels.level2]: <h2 className={className}>{children}</h2>,
    [TitleLevels.level3]: <h3 className={className}>{children}</h3>,
    [TitleLevels.level4]: <h4 className={className}>{children}</h4>,
    [TitleLevels.level5]: <h5 className={className}>{children}</h5>,
    [TitleLevels.level6]: <h6 className={className}>{children}</h6>,
  }[level];
};

export default PageTitle;
