import React from "react";
import Text from "./Text";
import classes from "./styles.module.scss";

interface IListProps {
  items: string[] | Element[];
  wrapperClasses?: string;
  itemClasses?: string;
  textClasses?: string;
}

const List: React.FC<IListProps> = ({
  items,
  wrapperClasses,
  itemClasses = "",
  textClasses = "",
}) => {
  return (
    <ul className={`${classes.ListWrapper} ${wrapperClasses}`}>
      {items.map((child, index) => (
        <li key={index} className={`${classes.ListItem} ${itemClasses}`}>
          <Text className={textClasses}>{child}</Text>
        </li>
      ))}
    </ul>
  );
};

export default List;
