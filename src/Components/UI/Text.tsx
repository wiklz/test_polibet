import React from "react";

interface ITextProps {
  className?: string;
}

const Text: React.FC<ITextProps> = ({ children, className }) => {
  return <p className={className}>{children}</p>;
};

export default Text;
