import React, { ReactNode } from "react";
import classes from "./styles.module.scss";

interface IImageCardProps {
  text?: ReactNode;
  src: string;
  alt?: string;
}

const ImageCard: React.FC<IImageCardProps> = ({
  text,
  src,
  alt = "card_img",
}) => {
  return (
    <div className={classes.Wrapper}>
      <div className={classes.Text}>{text}</div>
      <img className={classes.Image} src={src} alt={alt} />
    </div>
  );
};

export default ImageCard;
